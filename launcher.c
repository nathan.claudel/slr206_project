#include <stdio.h>
#include <stdlib.h>

#define BUFF_SIZE 256
#define FOR(i, min, max) for(int i = min; i < max; i++)

int thread_nums[]   = {1, 4, 8, 12};
int update_ratios[] = {0, 10, 100};
int list_sizes[]    = {10, 100, 1000};

int main(int argc, char **argv)
{
    if(argc != 2)
    {
        printf("Usage: ./launcher <algo>");
        return -1;
    }

    printf("number of threads, update ratio, list size, throughput\n");

    FOR(i, 0, 4) 
    {
        FOR(j, 0, 3)
        {
            FOR(k, 0, 3)
            {
                char *buffer = (char *) malloc(sizeof(char)*BUFF_SIZE);
                snprintf(buffer, BUFF_SIZE, 
                        "java -cp synchrobench/java/bin:bin contention.benchmark.Test -W 0 -d 2000 -b %s -t %d -u %d -i %d -r %d | grep Throughput | tr -d \"Throughput (ops/s):	\"",
                        argv[1],
                        thread_nums[i],
                        update_ratios[j],
                        list_sizes[k],
                        2*list_sizes[k]);
                printf("%d, %d, %d, ",
                        thread_nums[i],
                        update_ratios[j],
                        list_sizes[k]);
                fflush(stdout);
                //puts(buffer);
                system(buffer);
            }
        }
    }


    return 0;
}
