package slr206_project;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import contention.abstractions.AbstractCompositionalIntSet;

public class HandOverHandListSet extends AbstractCompositionalIntSet 
{
	// sentinel nodes
	private Node head;
	private Node tail;

	public HandOverHandListSet()
	{     
		head = new Node(Integer.MIN_VALUE);
		tail = new Node(Integer.MAX_VALUE);
		head.next = tail;
	}
	
	//Hand-over-hand locking
	private void lock()
	{
		Node curr = head;
		while(curr.key != tail.key)
		{
			curr.lock.lock();
			curr = curr.next;
		}
		tail.lock.lock();
	}
	
	private void unlock()
	{
		Node curr = head;
		while(curr.key != tail.key)
		{
			curr.lock.unlock();
			curr = curr.next;
		}
		tail.lock.unlock();
	}
	
	@Override
	public boolean addInt(int item){
		lock();
		try {  
			Node pred = head;
			Node curr = head.next;
			while (curr.key < item){
				pred = curr;
				curr = pred.next;
			}
			if (curr.key == item) {
				return false;
			} else {
				Node node = new Node(item);
				node.lock.lock();
				node.next=curr;
				pred.next=node;
				return true;
			}
		} finally{
			unlock();
		} 	 
	}

	
	@Override
	public boolean removeInt(int item){
		lock();
		try {  
			Node pred = head;
			Node curr = head.next;
			while (curr.key < item) {
				pred = curr;
				curr = pred.next;
			}
			if (curr.key == item) {
				pred.next = curr.next;
				curr.lock.unlock();
				return true;
			} else {
				return false;
			}
		} finally{
			unlock();
		} 
	}

	@Override
	public boolean containsInt(int item){
		lock();
		try {
			Node pred = head;
			Node curr = head.next;
			while (curr.key < item) {
				pred = curr;
				curr = pred.next;
			}
			if (curr.key == item){
				return true;
			} else {
				return false;
			}
		} finally{
			unlock();
		} 
	}

	private class Node {
		Node(int item) {
			key = item;
			next = null;
		}
		public int key;
		public Node next;
		public Lock lock = new ReentrantLock();
	}

	@Override
	public void clear() {
		head = new Node(Integer.MIN_VALUE);
		head.next = new Node(Integer.MAX_VALUE);
	}

	/**
	 * Non atomic and thread-unsafe
	 */
	@Override
	public int size() {
		int count = 0;

		Node curr = head.next;
		while (curr.key != Integer.MAX_VALUE) {
			curr = curr.next;
			count++;
		}
		return count;
	}
}
