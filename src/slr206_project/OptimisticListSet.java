package slr206_project;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import contention.abstractions.AbstractCompositionalIntSet;

public class OptimisticListSet extends AbstractCompositionalIntSet
{
	// sentinel nodes
	private Node head;
	private Node tail;

	public OptimisticListSet()
	{     
		head = new Node(Integer.MIN_VALUE);
		tail = new Node(Integer.MAX_VALUE);
		head.next = tail;
	}

	// Checks if the elements sequence pred, curr is accessible in the list
	private boolean validate(Node pred, Node curr)
	{
		Node node = head;
		while(node.key <= pred.key)
		{
			if(node.key == pred.key)
				return pred.next.key == curr.key;
			node = node.next;
		}
		return false;
	}

	@Override
	public boolean addInt(int item){
		while(true)
		{
			Node pred = head;
			Node curr = head.next;
			while (curr.key < item)
			{
				pred = curr;
				curr = curr.next;
			}
			pred.lock.lock();
			curr.lock.lock();
			try
			{
				if(validate(pred, curr))
				{
					if (curr.key == item) 
					{
						return false;
					} 
					else 
					{
						Node node = new Node(item);
						node.next=curr;
						pred.next=node;
						return true;
					}
				}
			}
			finally
			{
				pred.lock.unlock();
				curr.lock.unlock();
			}
		}
	}

	@Override
	public boolean removeInt(int item){
		while(true)
		{
			Node pred = head;
			Node curr = head.next;
			while (curr.key < item)
			{
				pred = curr;
				curr = curr.next;
			}
			pred.lock.lock();
			curr.lock.lock();
			try
			{
				if(validate(pred, curr))
				{
					if (curr.key == item) 
					{
						pred.next = curr.next;
						return true;
					} 
					else 
					{
						return false;
					}
				}
			}
			finally
			{
				pred.lock.unlock();
				curr.lock.unlock();
			}
		} 
	}

	@Override
	public boolean containsInt(int item){
		while(true)
		{
			Node pred = head;
			Node curr = head.next;
			while (curr.key < item)
			{
				pred = curr;
				curr = curr.next;
			}
			pred.lock.lock();
			curr.lock.lock();
			try
			{
				if(validate(pred, curr))
				{
					return curr.key == item;
				}
			}
			finally
			{
				pred.lock.unlock();
				curr.lock.unlock();
			}
		} 
	}

	private class Node {
		Node(int item) {
			key = item;
			next = null;
		}
		public int key;
		public Node next;
		public ReentrantLock lock = new ReentrantLock();
	}

	@Override
	public void clear() {
		head = new Node(Integer.MIN_VALUE);
		head.next = new Node(Integer.MAX_VALUE);
	}

	/**
	 * Non atomic and thread-unsafe
	 */
	@Override
	public int size() {
		int count = 0;

		Node curr = head.next;
		while (curr.key != Integer.MAX_VALUE) {
			curr = curr.next;
			count++;
		}
		return count;
	}
}
